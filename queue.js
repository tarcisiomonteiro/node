function MessageQueue(){
  this.pusher = new Pusher('key');
  this.items = [];
  this.timeWindow = 100; // 100 ms
}

// send messages every 100ms
MessageQueue.prototype.cycle = function(){
  var self = this;

  this.interval = setInterval(function(){
    if (self.items.length > 0 ) {
      var item = self.items.shift();
      self.pusher.channel(item.channel).trigger(item.event, item.data);
    }
  }, this.timeWindow);
};

MessageQueue.prototype.add = function(channel, event, data) {
  this.items.push({
    channel: channel,
    event: event,
    data: data
  });
};

// cleanup
MessageQueue.prototype.dispose = function(){
  this.clearInterval(this.interval);
  this.items = [];
};

var queue = new MessageQueue();
queue.cycle(); // start queue

// add to the queue
queue.add('test_channel', 'my_event', {hello: 'world'});