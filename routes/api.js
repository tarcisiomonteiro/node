//dependencies:
var express = require('express');
var router = express.Router();

//get models
var Device = require ('../models/device');
var Evolucao = require('../models/evolucao');
var Usuario = require('../models/usuario');
var Historico = require('../models/historico');
var Message = require('../models/message');


//routes:
Device.methods(['get', 'post', 'put', 'delete']);
Device.register(router, '/device');

Message.methods(['get', 'post', 'put', 'delete']);
Message.register(router, '/message');

Evolucao.methods(['get', 'post', 'put', 'delete']);
Evolucao.register(router, '/evolucao');

Historico.methods(['get', 'post', 'put', 'delete']);
Historico.register(router, '/historico');

Usuario.methods(['get', 'post', 'put', 'delete']);
Usuario.register(router, '/usuario');

//return router:
module.exports = router;

router.use(function(req, res, next) {
    // do logging
    console.log('Processando as informações.');
    next(); // make sure we go to the next routes and don't stop here
});

router.get('/', function(req, res) {
    res.json({ 'seja bem vindo': 'Agora você pode usar nossa api!' });   
});

//adicionando um novo device
router.route('/device').post(function(req, res) {
  var device = new Device(req.body);

  device.save(function(err) {
    if (err) {
      return res.send(err);
    }
    res.send({ message: 'device adicionado' });
  });
});

router.route('/usuario/:id').get(function(req, res) {
        Usuario.findOne(req.params.id, function(err, usuario) {
            if (err){
               res.send(err);
             } else{
            res.json(usuario);              
             }
        });
    });


