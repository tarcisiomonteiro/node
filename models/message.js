//dependencies
var restful = require('node-restful');
var mongoose = restful.mongoose;
//Schema:
var messageSchema = new mongoose.Schema({
  text: String,
  nome: String
});

//return models:
module.exports = restful.model('message', messageSchema);