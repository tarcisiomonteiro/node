//dependencies
var restful = require('node-restful');
var mongoose = restful.mongoose;
//Schema:
var evolucaoSchema = new mongoose.Schema({
	usuario: String,
	qtde_servicos: String,
	pontuacao: String,
	nivel: Number
});

//return models:
module.exports = restful.model('evolucao', evolucaoSchema);