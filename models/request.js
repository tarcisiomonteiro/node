//dependencies
var restful = require('node-restful');
var mongoose = restful.mongoose;

//Schema:
var requestSchema = new mongoose.Schema({
	data: String,
	endereco: String,
	horario: String,
	usuario: String,
	aceito: Boolean
});

//return models:
module.exports = restful.model('request', requestSchema);