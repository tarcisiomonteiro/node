//dependencies
var restful = require('node-restful');
var mongoose = restful.mongoose;

//Schema:
var usuarioSchema = new mongoose.Schema({
	nome: String,
	username: String,
	email: String,
	telefone: String,
	cidade: String,
	diarista: Boolean,
	nivel: String,
    disponibilidade: Array 
    
});

//return models:
module.exports = restful.model('usuario', usuarioSchema);