//dependencies
var restful = require('node-restful');
var mongoose = restful.mongoose;
//Schema:
var deviceSchema = new mongoose.Schema({
	device_id: String,
	perfil: String
});

//return models:
module.exports = restful.model('device', deviceSchema);