//dependencies
var restful = require('node-restful');
var mongoose = restful.mongoose;

//Schema:
var historicoSchema = new mongoose.Schema({
	mes: String,
	dia: String,
	cidade: String,
	usuario: String,
	usuario_contratante: String,
	usuario_diarista: String,
	realizado: Boolean,
	pontos_recebidos: Number
});

//return models:
module.exports = restful.model('historico', historicoSchema);