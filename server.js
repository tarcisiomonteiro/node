//dependencies
var http = require("http");
var Pusher = require('pusher');
var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');

//connecting to mongoLab
mongoose.connect ('mongodb://manager:click2010@ds019468.mlab.com:19468/diaristas/');

var pusher = new Pusher({
  appId: '200918',
  key: 'e79e62b00b043108460b',
  secret: 'bcb679275d119b514699',
  cluster: 'eu',
  encrypted: true
});

pusher.trigger('test_channel', 'my_event', {
  "message": "Chat Server UP!"
});

//express
var app = express();
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

//routes:
app.use('/api', require('./routes/api'));

app.set('port', (process.env.PORT || 5000));
//app.listen(5000);
app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});

// Generic error handler used by all endpoints.
function handleError(res, reason, message, code) {
  console.log("ERROR: " + reason);
  res.status(code || 500).json({"error": message});
}

app.post('/messages', function(req, res){
  var message = {
    text: req.body.text,
    name: req.body.name
  }
  pusher.trigger('chatroom', 'new_message', message);
  res.json({success: 200});
});

